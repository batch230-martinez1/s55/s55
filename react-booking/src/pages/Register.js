import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext, } from 'react';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
// import Logout from './logout';
export default function Register(){
  
  // State hooks to store the values of inpput fields
  // const { user } = useContext(UserContext);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNumber, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  // State to determine whether submit button is anabled or not
  const[isActive, setIsActive] = useState(false);
  const navigate = useNavigate();
  //  Check if values are successfully binded
  // console.log(email);
  // console.log(password1);
  // console.log(password2);
  
/* 

*/

  function registerUser(event){
      // Prevents page redirection via form submission
      event.preventDefault();
      fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
        method: 'POST',
        headers: {'Content-type' : 'application/json'},
        body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email: email,
            mobileNumber: mobileNumber,
            password: password1
        })
    })
    .then(res => res.json())
        .then(data =>{
            console.log(data);
            if(data!==null){            
              Swal.fire({
                title: "Registration Successful",
                icon: "success",
                text: "Welcome to Zuitt"               
            })
            navigate('/login');
          }
            else{
              Swal.fire({
                  title: "Duplicate email found",
                  icon: "error",
                  text: "Please provide a different email."
              })

          }
        })
   
     /*  setUser({
          email: localStorage.getItem('email')
      }) */
      //Clear input fields
      setFirstName('');
      setLastName('');
      setEmail('');
      setMobileNo('');
      setPassword1('');
      setPassword2('');
  }

  useEffect(() =>{
    if((firstName!==''&& lastName!==''&& mobileNumber!==''&& email!=='' && password1!==''&& password2 !=='')&&(password1===password2)){
      setIsActive(true);
    } 
    else{
      setIsActive(false);
    }
  },[firstName, lastName, mobileNumber, email, password1, password2])

  return(
  <Form onSubmit={(event)=> registerUser(event)}>
      <h3>Register</h3>
    <Form.Group controlId="userFirstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control 
          type="firstName" 
          placeholder="Enter first name" 
          value = {firstName}
          onChange = {event => setFirstName(event.target.value)}
          required
        />
    </Form.Group>
    <Form.Group controlId="userLastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control 
          type="lastName" 
          placeholder="Enter last name" 
          value = {lastName}
          onChange = {event => setLastName(event.target.value)}
          required
        />
    </Form.Group>
    <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
          type="email" 
          placeholder="Enter email" 
          value = {email}
          onChange = {event => setEmail(event.target.value)}
          required
        />
        <Form.Text className="text-muted">
            We'll never share your email with anyone else.
        </Form.Text>
    </Form.Group>
    <Form.Group controlId="userMobileNo">
    <Form.Label>Mobile Number</Form.Label>
    <Form.Control 
      type="mobileNo" 
      placeholder="Enter mobile number" 
      value={mobileNumber}
      onChange={event => setMobileNo(event.target.value)}
      pattern="\d{11}"
      required
    />
    <Form.Text className="text-muted">
      Please input 11 digits.
    </Form.Text>
   </Form.Group>
    <Form.Group controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control 
          type="password" 
          placeholder="Password" 
          value={password1}
          onChange={event => setPassword1(event.target.value)}
          required
        />
    </Form.Group>

    <Form.Group controlId="password2" className='mt-2'>
        <Form.Label>Verify Password</Form.Label>
        <Form.Control 
          type="password" 
          placeholder="Verify Password" 
          value={password2}
          onChange={e => setPassword2(e.target.value)}
          required
        />
    </Form.Group>
    {isActive ? //true
     <Button className='mt-3' variant="primary" type="submit" id="submitBtn">
     Submit
   </Button>
    :           //false
    <Button className='mt-3' variant="danger" type="submit" id="submitBtn" disabled>
      Submit
    </Button>
    }
</Form>
  )
}
