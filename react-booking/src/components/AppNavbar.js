import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import { Fragment, useContext } from 'react';
import UserContext from '../UserContext';

export default function AppNavbar(){
	// const [user, setUSer] = useState(localStorage.getItem("email"));
	const { user } = useContext(UserContext);
	// const [user, setUser] = useState(localStorage.getItem("email"));

	console.log(user);
  return(
    <Navbar bg="light" expand="lg">
		    <Container fluid>
		        <Navbar.Brand as={Link} to="/" href="#home">Zuitt Booking (from AppNavbar.js)</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		        <Navbar.Collapse id="basic-navbar-nav">
		            <Nav className="mr-auto">
		            <Nav.Link as={NavLink} to="/" >Home</Nav.Link>
		            <Nav.Link as={NavLink} to="/courses" >Courses</Nav.Link>								
								{(user.id !== null)?
									<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
									:
									<Fragment>
										<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
										<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
									</Fragment>
								}
								
		            </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
  )
}

/* export default function AppNavbar(){
	// const [user, setUSer] = useState(localStorage.getItem("email"));

	const [user, setUser] = useState({ email: localStorage.getItem("email") || "" });

	console.log(user);
  return(
    <Navbar bg="light" expand="lg">
		    <Container fluid>
		        <Navbar.Brand as={Link} to="/" href="#home">Zuitt Booking (from AppNavbar.js)</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		        <Navbar.Collapse id="basic-navbar-nav">
		            <Nav className="mr-auto">
		            <Nav.Link as={NavLink} to="/" >Home</Nav.Link>
		            <Nav.Link as={NavLink} to="/courses" >Courses</Nav.Link>
								<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
								{(user.email !== null)?
									<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
									:
									<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
								}
								
		            </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
  )
}
 */
